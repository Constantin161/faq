<?php 
namespace brovkov\app;

use brovkov\app\PageController;
use brovkov\app\Request;

include_once('base/Request.php');

class Routing
{
    static function start()
    {
        $request = new Request;
        $query_string_params = $request->getBody("GET");

        // контролер и экшн по умолчанию
        $controller_name = 'Page';
        $action_name = 'index';

        // получаем имя контроллера
        if ( !empty($query_string_params['controller']))
        {
            $controller_name = ucfirst($query_string_params['controller']);
        }

        // получаем имя экшена
        if ( !empty($query_string_params['action']))
        {
            $action_name = $query_string_params['action'];
        }

        // добавляем префиксы
        $controller_name .= 'Controller';
        $action_name .= 'Action';

        // Пытаемся подключить файл контроллера
        $path = '*'. DIRECTORY_SEPARATOR .'controller'. DIRECTORY_SEPARATOR. $controller_name.'.php';
        foreach (glob($path) as $path) {
            include_once $path;
            $is_include = true;
        }

        // если файл контролера не подключен выводим 404
        if(!isset($is_include)) {
            Routing::ErrorPage404();
        }

        // добавляем пространство имен.
        $controller_name = __NAMESPACE__."\\".$controller_name;

        // создаем контроллер
        $controller = new $controller_name;
        $action = $action_name;

        if(method_exists($controller, $action))
        {
            // вызываем действие контроллера
            $controller->$action();
        }
        else
        {
            Routing::ErrorPage404();
        }
    }

    function ErrorPage404()
    {
        header('HTTP/1.1 404 Not Found');
    }
}