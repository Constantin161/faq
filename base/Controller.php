<?php

namespace brovkov\app;

use Twig_Loader_Filesystem;
use Twig_Environment;
use brovkov\app\Request;

include_once('vendor/autoload.php');
include_once('base/Request.php');

class Controller
{
    protected $loader;

    protected $twig;

    protected $request;

    protected $data_filter = array(
        'author' => 'Имя',
        'email' => 'Электронный адрес',
        'description' => 'Вопрос',
        'login' => 'Логин',
        'password' => 'Пароль',
        );

    public function __construct()
    {
        $this->loader = new Twig_Loader_Filesystem('faq/templates');
        $this->twig = new Twig_Environment($this->loader);
        $this->request = new Request;  
    }

    public static function clearStr($str) 
    {
        $str=strip_tags($str);
        $str=stripslashes($str);
        $str=htmlspecialchars($str);
        $str=trim($str);
        return $str;
    }

    public function get_empty_fields($arr) {
        $notFilledFields = array_filter($arr, function($val) {
            return empty($val);
        });
        foreach($notFilledFields as $key => $value) {
            $notFilledFields[$key] = $this->data_filter[$key];
        }
        return $notFilledFields;
    }
}