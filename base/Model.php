<?php

namespace brovkov\app;

use PDO;

include_once('config.php');

class Model
{
    protected $pdo;

    protected $table_name;

    public function __construct()
    {
        $this->pdo = new PDO(DB, LOGIN, PW);
        $this->pdo->exec("SET NAMES utf8;");
    }

    public function findAll()
    {
        $sql = "SELECT * FROM ".$this->table_name;
        $stm = $this->pdo->prepare($sql);
        $stm->execute();
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public function findById($id)
    {
        $sql = "SELECT * FROM ".$this->table_name." WHERE id = ?";
        $stm = $this->pdo->prepare($sql);
        $stm->execute(array($id));
        return $stm->fetch(PDO::FETCH_ASSOC);
    }

    public function findBy($column, $value)
    {
        $sql = "SELECT * FROM ".$this->table_name." WHERE ".$column." = ?";
        $stm = $this->pdo->prepare($sql);
        $stm->execute(array($value));
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete($id)
    {
        $sql = "DELETE FROM ".$this->table_name." WHERE id = ?";
        $stm = $this->pdo->prepare($sql);
        return $stm->execute(array($id));
    }

    public function change($column, $value, $id)
    {
        $sql = "UPDATE ".$this->table_name." SET ".$column." = ? WHERE id = ?";
        $stm = $this->pdo->prepare($sql);
        return $stm->execute(array($value, $id));        
    }
}