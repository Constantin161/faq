<?php
namespace brovkov\app;

class Request 
{
    protected $target;

    protected $headers;

    protected $method;

    protected $body = array();

    protected $query_string_params;

    public function __construct()
    {
        foreach($_SERVER as $key=>$header){
            if(mb_strpos($key, "HTTP_") !== false)
            {
                $key = str_replace(array("HTTP_" , "_"), array("" , "-"), $key);
                $key = mb_convert_case($key, MB_CASE_TITLE);
                $this->headers[$key] = $header;
            }
        }

        // Получаем цель
        $this->target = $_SERVER['REQUEST_URI'];

        // Получаем метод запроса
        $this->method = $_SERVER['REQUEST_METHOD'];

        // Получаем тело запроса (POST параметры)
        foreach ($_POST as $key => $value) {
            $this->body[$key] = $this->clearStr($value);
        }

        // Получаем GET параметры
        foreach ($_GET as $key => $value) {
            $this->query_string_params[$key] = $this->clearStr($value);
        }
    }

    public function getRequestTarget()
    {
        return $this->target;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function getHeader($header)
    {
        if (isset($this->headers[$header]))
        {
            return $this->headers[$header];
        }
        return null;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    public function getBody($method)
    {
        if($method === "POST"){
            return $this->body;
        }
        if($method === "GET"){
            return $this->query_string_params;
        }
        return false;
    }

    public static function clearStr($str)
    {
        $str=strip_tags($str);
        $str=stripslashes($str);
        $str=htmlspecialchars($str);
        $str=trim($str);
        return $str;
    }
}