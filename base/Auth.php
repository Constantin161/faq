<?php

namespace brovkov\app;

use brovkov\app\AdminModel;

include_once('faq/model/AdminModel.php');

class Auth 
{
    private $login;

    private $pw;

    private $admin;

    private $adminModel;

    public function __construct($login, $pw)
    {
        $this->login = $login;
        $this->pw = md5($pw."brovkot");
        $this->adminModel = new AdminModel;
        $this->admin = $this->adminModel->findBy('login', $login);
    }

    public function sing_in()
    {
        if (empty($this->login) || empty($this->pw)) {
            return "Ошибка входа. Введите все необходимые данные!";
            }
        if(($this->login == $this->admin[0]['login']) && ($this->pw == $this->admin[0]['password'])) {
            $_SESSION['login'] = $this->login;
            $_SESSION['user_id'] = $this->admin['id'];
            return true;
        }
        else {
        	return "Ошибка входа. Введены неверные данные!";
        }
    }

    public static function is_auth()
    {
        if (isset($_SESSION['login'])) return true;
        else return false;
    }
}
