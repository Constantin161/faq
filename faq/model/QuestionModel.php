<?php
namespace brovkov\app;

use PDO;
use brovkov\app\Model;

include_once('base/Model.php');

class QuestionModel extends Model 
{
    const STATUS_WAIT = 1;

    const STATUS_POST = 2;

    const STATUS_HIDE = 3;

    const SQL = "SELECT q.id, q.description, q.id_topic, q.id_status, q.author, q.email, q.answer, q.crdate, t.description topic, s.description status
                FROM question q
                JOIN topic t ON t.id = q.id_topic
                JOIN status s ON s.id = q.id_status
                ORDER BY crdate DESC";
    
    protected $table_name = "question";
    
    public function findByTopic($topic)
    {
        $sql = self::SQL."WHERE t.discription = {$topic}";
        $stm = $this->pdo->prepare($sql);
        $stm->execute();
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public function findAll()
    {
        $sql = self::SQL;
        $stm = $this->pdo->prepare($sql);
        $stm->execute();
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public function createQuestion($question)
    {
        $sql = "INSERT INTO question (author, email, id_topic, description)
                VALUES (?, ?, ?, ?)";
        $stm = $this->pdo->prepare($sql);
        return $stm->execute($question);
    }

    public function corectQuestion($question) 
    {
        $sql = "UPDATE question SET id_topic = ?, id_status =?, author = ?, email = ?, description = ?, answer = ? WHERE id = ?";
        $stm = $this->pdo->prepare($sql);
        return $stm->execute($question);
    }
}