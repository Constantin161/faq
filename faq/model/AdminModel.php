<?php

namespace brovkov\app;

use brovkov\app\Model;
use PDO;

include_once('base/Model.php');

class AdminModel extends Model
{
    protected $table_name = "admin";

    public function createAdmin($admin)
    {
    	$sql = "INSERT INTO admin (login, password)
    	        VALUES (?, ?)";
    	$stm = $this->pdo->prepare($sql);
        return $stm->execute($admin);
    }
}