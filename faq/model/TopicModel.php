<?php

namespace brovkov\app;

use brovkov\app\Model;
use PDO;

include_once('base/Model.php');

class TopicModel extends Model
{
    protected $table_name = "topic";

    const SQL ="SELECT t.id, t.description, total.total, w.wait, p.post, h.hidden FROM topic t
    LEFT JOIN
    (SELECT id_topic, COUNT(id) total FROM question GROUP BY id_topic) total
    ON t.id = total.id_topic
    LEFT JOIN
    (SELECT * FROM (SELECT id_topic, id_status, COUNT(id) wait FROM question GROUP BY id_topic, id_status) statistic WHERE id_status = 1) w
    ON t.id = w.id_topic
    LEFT JOIN
    (SELECT * FROM (SELECT id_topic, id_status, COUNT(id) post FROM question GROUP BY id_topic, id_status) statistic WHERE id_status = 2) p
    ON t.id = p.id_topic
    LEFT JOIN
    (SELECT * FROM (SELECT id_topic, id_status, COUNT(id) hidden FROM question GROUP BY id_topic, id_status) statistic WHERE id_status = 3) h
    ON t.id = h.id_topic";
    
    public function findAll()
    {
        $sql = self::SQL;
        $stm = $this->pdo->prepare($sql);
        $stm->execute();
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }


    public function createTopic($topic) 
    {
        $sql = "INSERT INTO topic (description)
                VALUES (?)";
        $stm = $this->pdo->prepare($sql);
        return $stm->execute($topic);
    }

}