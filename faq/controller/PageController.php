<?php
namespace brovkov\app;

use brovkov\app\QuestionModel;
use brovkov\app\TopicModel;
use brovkov\app\AdminModel;
use brovkov\app\StatusModel;
use brovkov\app\Auth;
use brovkov\app\Controller;

include_once('faq/model/QuestionModel.php');
include_once('faq/model/TopicModel.php');
include_once('faq/model/AdminModel.php');
include_once('faq/model/StatusModel.php');
include_once('base/Auth.php');
include_once('base/Controller.php');

class PageController extends Controller
{
    private $is_auth;

    public function __construct()
    {
        parent::__construct();
        $this->is_auth = Auth::is_auth();
    }

    public function indexAction()
    {
        $questionModel = new QuestionModel;
        $topicModel = new TopicModel;

        // Получаем все POST данные
        $request_body = $this->request->getBody("POST");
        $id = isset($request_body['id']) ? $request_body['id'] : null;
        $action = isset($request_body['action']) ? $request_body['action'] : null;
        
        // выполняем соответствующие нажатой кнопке действия
        if($action == "delete") {
            $questionModel->delete($id);
            }
        elseif($action == "hide") {
            $questionModel->change("id_status", QuestionModel::STATUS_HIDE, $id);
        }
        elseif($action == "post") {
            $questionModel->change("id_status", QuestionModel::STATUS_POST, $id);
        }

        // выводим страницу
        $questions = $questionModel->findAll();
        $topics = $topicModel->findAll();
        $params = array(
            'questions' => $questions,
            'topics' => $topics,
            );
        $this->showPage('question.tpl', $params);
    }

    public function createAction()
    {
        $questionModel = new QuestionModel;
        $topicModel = new TopicModel;

        // Получаем все POST данные
        $question = $this->request->getBody("POST");

        //проверяем на пустоту поля формы и формируем сообщение об ошибке
        $empty_fields = $this->get_empty_fields($question);
        if(!empty($empty_fields)) {
            $error_message = "Заполните следующие поля: ".implode(', ', $empty_fields);
        }

        // если все заполнено - создаем запись
        elseif(!empty($question)) {
            $question = array_values($question);
            $questionModel->createQuestion($question);
            header('Location: index.php');
        }

        // выводим страницу
        $topics = $topicModel->findAll();
        $params = array(
            'question' => $question,
            'topics' => $topics,
            'error_message' => isset($error_message) ? $error_message : null
            );
        $this->showPage('create.tpl', $params);
    }

    public function corectAction()
    {
        // если поьзователь не вошел как адним - выдаем ошибку доступа
        if(!$this->is_auth) {
            http_response_code(403);
            $this->showPage('403.tpl');
            die;
        }
        $questionModel = new QuestionModel;
        $topicModel = new TopicModel;
        $statusModel = new StatusModel;

        // Получаем все GET и POST данные
        $request_body = $this->request->getBody("POST");
        $id_status = isset($request_body['id_status']) ? $request_body['id_status'] : null;
        $query_string_params = $this->request->getBody("GET");
        $id = $query_string_params['id'];

        // получаем корректируемый вопрос, статусы и темы
        $question = $questionModel->findById($id);
        $statuses = $statusModel->findAll();
        $topics = $topicModel->findAll();
        
        // автоматическая смена статуса вопроса при заполнении поля Ответ
        if(!empty($request_body['answer']) && $id_status == QuestionModel::STATUS_WAIT) {
            $request_body['id_status'] = QuestionModel::STATUS_POST;
            }

        // проверяем, была ли нажата кнопка "Сохранить" и выполняем соответствующие действия
        $action = isset($request_body['action']) ? $request_body['action'] : null;
        
        // ограничиваем возможность опубликовать вопрос без ответа
        if(empty($request_body['answer']) && $id_status == QuestionModel::STATUS_POST)
            {
            $message = "Не возможно опубликовать вопрос без ответа. Заполните поле Ответ";
            }
        elseif (!empty($action)) {
            array_pop($question);
            $question['id'] = array_shift($question);
            
            // подгатавливаем массив для замены
            foreach ($question as $key => $value) {
                $question[$key] = !empty($request_body[$key]) ? $request_body[$key] : $value;
            }            
            $question = array_values($question);
            $questionModel->corectQuestion($question);
            header('Location: index.php');
        }

        // выводим страницу
        $params = array(
            'message' => isset($message) ? $message : null,
            'question' => $question,
            'topics' => $topics,
            'statuses' => $statuses,
            );
        $this->showPage('corect.tpl', $params);
    }

    public function adminAction()
    {
        // если поьзователь не вошел как адним - выдаем ошибку доступа
        if(!$this->is_auth) {
            http_response_code(403);
            $this->showPage('403.tpl');
            die;
        }
        $adminModel = new AdminModel;
        
        // Получаем все POST данные
        $request_body = $this->request->getBody("POST");
        $id = isset($request_body['id']) ? $request_body['id'] : null;
        $action = isset($request_body['action']) ? $request_body['action'] : null;

        // выполняем соответствующие нажатой кнопке действия
        if($action == "delete") {
            $adminModel->delete($id);
            }
        elseif($action == "change_pw") {
            if(!empty($password)) {
                $password = $this->clearStr(md5($request_body['new_pw']."brovkot"));
                $adminModel->change("password", $password, $id);
            }
            else {
                $message['password'] = "Пароль не может быть пустым";
            }
        }
        elseif($action == "create") {
            $newadmin['login'] = $request_body['login'];
            $newadmin['password'] = $request_body['password'];
            $empty_fields = $this->get_empty_fields($newadmin);
            if ($adminModel->findBy('login', $newadmin['login'])) {
                $message['create'] = "Администратор с таким логином уже существует!";
                $create_error = 1;
            }
            if (!empty($empty_fields)) {
                $message['create'] = "Заполните следующие поля: ".implode(', ', $empty_fields);
                $create_error = 1;
            }
            if (empty($create_error)) {
                $adminModel->createAdmin(
                    array(
                    $newadmin['login'],
                    md5($newadmin['password']."brovkot"))
                );
                $message['create'] = "Пользователь успешно создан!";
                unset($newadmin);
            }
        }
        
        // выводим страницу
        $admins = $adminModel->findAll();
        $params = array(
            'admins' => $admins,
            'message' => isset($message) ? $message : null,
            'newadmin' => isset($newadmin) ? $newadmin : null,
            );
        $this->showPage('admin.tpl', $params);
    }

    public function topicAction() 
    {
        // если поьзователь не вошел как адним - выдаем ошибку доступа
        if(!$this->is_auth) {
            http_response_code(403);
            $this->showPage('403.tpl');
            die;
        }
        $topicModel = new TopicModel;

        // Получаем все POST данные
        $request_body = $this->request->getBody("POST");
        $id = isset($request_body['id']) ? $request_body['id'] : null;
        
        // выполняем соответствующие нажатой кнопке действия
        $action = isset($request_body['action']) ? $request_body['action'] : null;
        if($action == "delete") {
            $topicModel->delete($id);
            }            
        if($action == "create") {
            $newtopic = $this->clearStr($request_body['description']);
            if (empty($newtopic)) {
                $message = "Заполните поле Тема!";
                $create_error = 1;
            }
            if ($topicModel->findBy('description', $newtopic)) {
                $message = "Тема с таким именем уже существует";
                $create_error = 1;
            }
            if (empty($create_error)) {
                $topicModel->createTopic(array($newtopic));
            }
        }
        
        // выводим страницу
        $topics = $topicModel->findAll();
        $params = array(
            'topics' => $topics,
            'message' => isset($message) ? $message : null,
            'newtopic' => isset($newadmin) ? $newadmin : null,
            );
        $this->showPage('topic.tpl', $params);
    }

    public function authAction()
    {
        $request_body = $this->request->getBody("POST");
        $login = isset($request_body['login']) ? $this->clearStr($request_body['login']) : null;
        $pw = isset($request_body['pw']) ? $this->clearStr($request_body['pw']) : null;
        $auth = new Auth($login, $pw);
        $message = isset($request_body['login']) ? $auth->sing_in() : "Введите данные, необходимые для входа администратора";
        
        // если не авторизирован админ выводим страницу
        if(!isset($_SESSION['login']))
        {
            $params = array(
                'message' => $message,
                );
            $this->showPage('auth.tpl', $params); 
        }
        else header('Location: index.php?page=admin');
    }

    public function exitAction()
    {
        session_destroy();
        $this->is_auth = false;
        header('Location: index.php');
    }
    
    private function showPage($page, $params = array()) 
    {
        $template = $this->twig->loadTemplate($page);
        $params['is_auth'] = $this->is_auth;
        $template->display($params);
    }
}