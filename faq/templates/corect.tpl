{% extends 'base.tpl' %}
	
	{% block body %}
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<form role="form" method="post">
					<div class="form-group">
			   			<label for="author">Имя</label>
			    		<input type="text" class="form-control" name="author" id="author" value="{{ question.author }}">
			  		</div>
			  		<div class="form-group">
			   			<label for="email">Электронный адрес</label>
			    		<input type="text" class="form-control" name="email" id="email" value="{{ question.email }}">
			  		</div>
			  		<div class="form-group">
			   			<label for="topic">Категория</label>
			    		<select class="form-control" name="id_topic" id="topic">
							{% for topic in topics %}
							<option {% if topic.id == question.id_topic %} selected {% endif %} value="{{ topic.id }}">
								{{ topic.description }}
							</option>
							{% endfor %}
						</select>
			  		</div>
			  		<div class="form-group">
			   			<label for="description">Вопрос</label>
			    		<textarea class="form-control" name="description" id="description">{{ question.description }}</textarea>
			  		</div>
			  		<div class="form-group">
			   			<label for="answer">Ответ</label>
			    		<textarea class="form-control" name="answer" id="answer" placeholder="Введите ответ">{{ question.answer }}</textarea>
			  		</div>
			  		<div class="form-group">
			   			<label for="status">Статус</label>
			    		<select class="form-control" name="id_status" id="status">
							{% for status in statuses %}
							<option {% if status.id == question.id_status %} selected {% endif %} value="{{ status.id }}">
								{{ status.description }}
							</option>
							{% endfor %}
						</select>
			  		</div>
			  		<input type="hidden" name="action" value="corect">
					<input type="hidden" name="id" value="{{ question.id }}">
					{{ message }} <br>
			  		<button class="btn btn-default" type="submit">Сохранить</button>
				</form>
			</div>
		</div>
	</div>
	{% endblock %}

	