{% extends 'base.tpl' %}

	{% block body %}
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h4>{{ message }}</h4> 
			</div>
			<div class="col-md-12">
				<form role="form" method="post">
					<div class="form-group">
			   			<label for="login">Логин</label>
			    		<input type="text" class="form-control" name="login" id="login" placeholder="Введите логин">
			  		</div>
			  		<div class="form-group">
			   			<label for="password">Пароль</label>
			    		<input type="password" class="form-control" name="pw" id="password" placeholder="Введите пароль">
			  		</div>
			  		<button class="btn btn-default" type="submit">Войти</button>
				</form>
			</div>
		</div>
	</div>
	{% endblock %}
