{% extends 'base.tpl' %}
	
	{% block body %}
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				 <table class="table table-hover">
					<thead>
						<tr>
							<th>Логин</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					{% for admin in admins %}
						<tr>
							<td>{{ admin.login }}</td>
							<td>
								<form method="post">
									<input type="hidden" name="id" value="{{ admin.id }}">
									<input type="hidden" name="action" value="delete">
									<button class="btn btn-default" type="submit">Удалить</button>
								</form>
							</td>
							<form method="post">
							<td>
								<input type="text" class="form-control" name="new_pw" placeholder="Введите новый пароль">
								<input type="hidden" name="id" value="{{ admin.id }}">
								<input type="hidden" name="action" value="change_pw">
							</td>
							<td>	
								<button class="btn btn-default" type="submit">Сменить пароль</button>
							</td>
			  				</form>
						</tr>
					{% endfor %}
					</tbody>
				</table>
				{{ message.password }}
			</div>
			<div class="col-md-12">
				<h4>Создать нового пользователя</h4>
				{{ message.create }}
				<form role="form" method="post">
					<div class="form-group">
			   			<label for="author">Логин</label>
			    		<input type="text" class="form-control" name="login" id="login" placeholder="Введите логин" value="{{ newadmin.login }}" >
			  		</div>
			  		<div class="form-group">
			   			<label for="password">Пароль</label>
			    		<input type="password" class="form-control" name="password" id="password" placeholder="Введите пароль" value="{{ newadmin.password }}">
			  		</div>
			  		<input type="hidden" name="action" value="create">
			  		<button class="btn btn-default" type="submit">Создать</button>
				</form>
			</div>
		</div>
	</div>
	{% endblock %}