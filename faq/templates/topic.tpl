{% extends 'base.tpl' %}
	
	{% block body %}
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				 <table class="table table-hover">
					<thead>
						<tr>
							<th>Тема</th>
							<th>
								Количество вопросов<br>
								общее(без ответа/опубликованных/скрытых)
							</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					{% for topic in topics %}
						<tr>
							<td>{{ topic.description }}</td>
							<td>{{ topic.total ? topic.total : "0"  }} ({{ topic.wait ? topic.wait : "-" }}  / {{ topic.post ? topic.post : "-" }} / {{ topic.hidden ? topic.hidden : "-" }}) </td>
							<td>
								<form method="post">
									<input type="hidden" name="id" value="{{ topic.id }}">
									<input type="hidden" name="action" value="delete">
									<button class="btn btn-default" type="submit">Удалить</button>
								</form>
							</td>
						</tr>
					{% endfor %}
					</tbody>
				</table>
			</div>
			<div class="col-md-12">
				<h4>Создать новую тему</h4>
				{{ message }}
				<form role="form" method="post">
					<div class="form-group">
			   			<label for="description">Тема</label>
			    		<input type="text" class="form-control" name="description" id="description" placeholder="Введите название темы" value="{{ newtopic }}" >
			  		</div>
			  		<input type="hidden" name="action" value="create">
			  		<button class="btn btn-default" type="submit">Создать</button>
				</form>
			</div>
		</div>
	</div>
	{% endblock %}