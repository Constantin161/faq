{% extends 'base.tpl' %}
	
	{% block body %}
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>Новй вопрос</h2> 
			</div>
			<div class="col-md-12">
				<form role="form" method="post">
					<div class="form-group">
			   			<label for="author">Имя</label>
			    		<input type="text" class="form-control" name="author" id="author" placeholder="Введите Ваше имя" value="{{ question.author }}">
			  		</div>
			  		<div class="form-group">
			   			<label for="email">Электронный адрес</label>
			    		<input type="text" class="form-control" name="email" id="email" placeholder="Введите Ваш электронный адрес" value="{{ question.email }}">
			  		</div>
			  		<div class="form-group">
			   			<label for="topic">Категория</label>
			    		<select class="form-control" name="topic" id="topic">
							{% for topic in topics %}
							<option value="{{ topic.id }}">{{ topic.description }}</option>
							{% endfor %}
						</select>
			  		</div>
			  		<div class="form-group">
			   			<label for="description">Вопрос</label>
			    		<textarea class="form-control" name="description" id="description" placeholder="Введите Ваш вопрос">{{ question.description }}</textarea>
			  		</div>
			  		
			  		<button class="btn btn-default" type="submit">Создать</button>
				</form>
			</div>
			<div class="col-md-12">
				{{ error_message }}
			</div>
		</div>
	</div>
	{% endblock %}