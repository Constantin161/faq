{% extends 'base.tpl' %}
	
	{% block body %}
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<nav class="navmenu navmenu-default" role="navigation">
					<ul class="nav navmenu-nav">
						{% for topic in topics %}
							<li><a href="#">{{ topic.description }}</a></li>
						{% endfor %}
					</ul>
				</nav>
			</div>
			<div class="col-md-9">
				{% if is_auth %}
				<ul class="nav nav-tabs">
					<li class="active"><a href="#post" data-toggle="tab">Опубликованные</a></li>
					<li><a href="#noanswer" data-toggle="tab">Ожидают ответа</a></li>
					<li><a href="#hidden" data-toggle="tab">Скрытые</a></li>
				</ul>

				<div class="tab-content">
  					<div class="tab-pane active" id="post">
  						<br>
  				{% endif %}
  						{% for topic in topics %}
  						<h4>{{topic.description}}</h4>
  						{% if topic.post is null %}
  						В данном разделе пока отсутствуют опубликованные вопросы
  						{% endif %}
						<div class="panel-group" id="accordion">
							{% for question in questions %}	
							{% if (question.id_topic == topic.id) and (question.status == "Опубликован") and (question.answer is not null) %}	
							<div class="panel panel-default">
								<div class="panel-heading">
							  		<h4 class="panel-title">
					          			<a data-toggle="collapse" data-parent="#accordion" href="#collapse{{ question.id }}">
					          				<strong>{{ question.author}}:</strong> <br>
					          				{{ question.description | nl2br }}<br>
					          				{{ question.crdate|date("d.m.Y H:i:s") }}<br>
					            		</a>
				          			</h4>
				          			{% if is_auth %}
							  		<ul class="list-inline">
							  			<li>
							  				<a href="index.php?controller=page&action=corect&id={{ question.id}}" class="btn btn-default" role="button">Редактировать</a>
							  			</li>
							  			<li>
							  				<form role="form" method="post">
													<input type="hidden" name="action" value="delete">
													<input type="hidden" name="id" value="{{ question.id }}">
						  							<button class="btn btn-default" type="submit">Удалить</button>
					  						</form>
							  			</li>
							  			<li>
							  				<form role="form" method="post">
													<input type="hidden" name="action" value="hide">
													<input type="hidden" name="id" value="{{ question.id }}">
							  						<button class="btn btn-default" type="submit">Скрыть</button>
						  					</form>
							  			</li>
							  		</ul>
						  			{% endif %}
								</div>
				
								<div id="collapse{{ question.id }}" class="panel-collapse collapse">
								  	<div class="panel-body">
								   		{{ question.answer }}
								  	</div>
								</div>
							</div>
							{% endif %}
							{% endfor %}
						</div>
						{% endfor %}	
 				{% if is_auth %}
 					</div>
	 				<div class="tab-pane" id="noanswer">
	 					<table class="table table-hover">
		 				{% for question in questions %}
		 					{% if (question.status == "Ожидает ответа") or (question.answer is null) %}
		 					<tr>
								<td>
									<strong>{{ question.author}}:</strong><br>
									{{ question.description | nl2br }} <br>
									{{ question.crdate|date("d.m.Y H:i:s") }} <br>
									Категория: {{ question.topic }}   
								</td>
								<td>
									<a href="index.php?controller=page&action=corect&id={{ question.id}}" class="btn btn-default" role="button">Редактировать</a>
								</td>
								<td>
									<form role="form" method="post">
										<input type="hidden" name="action" value="delete">
										<input type="hidden" name="id" value="{{ question.id }}">
				  						<button class="btn btn-default" type="submit">Удалить</button>
			  						</form>
								</td>
								<td></td>
							</tr>
							{% endif %}
		 				{% endfor %}
		 				</table>
	 				</div>

					<div class="tab-pane" id="hidden">
						<table class="table table-hover">
		 				{% for question in questions %}
		 					{% if question.status == "Скрыт" %}
		 					<tr>
								<td>
									<strong>{{ question.author}}:</strong><br>
									{{ question.description | nl2br }} <br>
									{{ question.crdate|date("d.m.Y H:i:s") }} <br>
									Категория: {{ question.topic }}
								</td>
								<td>
									<a href="index.php?controller=page&action=corect&id={{ question.id}}" class="btn btn-default" role="button">Редактировать</a>
								</td>
								<td>
									<form role="form" method="post">
										<input type="hidden" name="action" value="delete">
										<input type="hidden" name="id" value="{{ question.id }}">
				  						<button class="btn btn-default" type="submit">Удалить</button>
			  						</form>
								</td>
								<td>
									<form role="form" method="post">
										<input type="hidden" name="action" value="post">
										<input type="hidden" name="id" value="{{ question.id }}">
				  						<button class="btn btn-default" type="submit">Опубликовать</button>
			  						</form>
								</td>
							</tr>
							{% endif %}
		 				{% endfor %}
		 				</table>
					</div>
				</div>
	 			{% endif %}
			</div>
		</div>
	</div>

	{% endblock %}
