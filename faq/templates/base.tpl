<!DOCTYPE html>
<html>
    <head>
       <meta charset="utf-8">
       <title>FAQ</title>
            <link rel="stylesheet" type="text/css" href="static/css/bootstrap.css" />
            <link rel="stylesheet" type="text/css" href="static/css/bootstrap-theme.css" />
            <link rel="stylesheet" type="text/css" href="static/css/main.css" />
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
            <script src="static/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <nav role="navigation" class="navbar navbar-default">
                  <div class="navbar-header">
                      <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                      <a class="navbar-brand logo" href="#"><img src="static/img/cat1.jpg"></a>
                  </div>
                  {% if is_auth %}
                  <div id="navbarCollapse" class="collapse navbar-collapse">
                      <ul class="nav navbar-nav">
                        <li><a href="index.php?controller=page&action=index">Вопросы</a></li>
                        <li><a href="index.php?controller=page&action=topic">Темы</a></li>
                        <li><a href="index.php?controller=page&action=admin">Администраторы</a></li>
                      </ul>
                      <ul class="nav navbar-nav navbar-right">
                        <li><a href="index.php?controller=page&action=exit">Выйти</a></li>
                      </ul>
                  </div>
                  {% else %}
                  <div id="navbarCollapse" class="collapse navbar-collapse">
                      <ul class="nav navbar-nav">
                        <li><a href="index.php?controller=page&action=index">Вопросы</a></li>
                        <li><a href="index.php?controller=page&action=create">Создать</a></li>
                      </ul>
                      <ul class="nav navbar-nav navbar-right">
                        <li><a href="index.php?controller=page&action=auth">Войти</a></li>
                      </ul>
                  </div>
                  {% endif %}
              </nav>
            </div>
          </div>
        </div>

        {% block body %}{% endblock %}
    </body>
</html>